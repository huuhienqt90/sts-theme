</div><!-- END .container -->
<footer>
	<img src="<?php echo get_template_directory_uri() . '/images/footer-bg.jpg'; ?>" width="100%" />
	<div class="footer-section">
	<?php if ( is_active_sidebar( 'footer-area' ) ) : ?>
		<div class="container">
			<div class="row">
				<?php dynamic_sidebar( 'footer-area' ); ?>
			</div>
		</div><!-- END .container -->		
	<?php endif; ?>
	</div><!-- END .footer-section -->
	<div class="footer-coppy">

	</div><!-- END .footer-coppy -->
</footer>
<?php wp_footer(); ?>
</body>
</html>