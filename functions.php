<?php
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}
if( ! class_exists('STS_Theme_Settup') ):
	class STS_Theme_Settup{
		function __construct(){
			add_action( 'after_setup_theme', array( $this, 'theme_init' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'theme_scripts' ) );
			add_action( 'widgets_init', array( $this, 'widgets_init' ) );
		}
		function theme_init(){
			add_theme_support( 'title-tag' );
			add_theme_support( 'html5', array(
				'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
			) );
			add_theme_support( 'automatic-feed-links' );
			show_admin_bar( false );
			register_nav_menus( array(
				'primary' => __( 'Primary Menu',      'sts-theme' ),
			) );
		}
		function theme_scripts(){			
			wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
			wp_enqueue_style( 'bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css' );
			wp_enqueue_style( 'style-name', get_stylesheet_uri() );
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'boostrap', get_template_directory_uri() . '/js/bootstrap.min.js' );
		}
		function widgets_init(){
			register_sidebar( array(
				'name'          => __( 'Footer Area', 'twentyfifteen' ),
				'id'            => 'footer-area',
				'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
				'before_widget' => '<div id="%1$s" class="widget col-md-4 %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			) );
		}
	}
	new STS_Theme_Settup();
endif;